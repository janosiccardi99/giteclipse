package InterfazGrafica;

import java.awt.BorderLayout;
 

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Negocio.GrafoPersonas;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GrafoVisual extends JDialog {

	private boolean kruskal=false;
	private int dividido=0;
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	public JPanel panel1;

	public static void main(String[] args) {
		try {
			GrafoVisual dialog = new GrafoVisual(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public GrafoVisual(GrafoPersonas datos) {
		setBounds(100, 100, 1004, 777);		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new lienzo(contentPanel,datos);
		panel.setBounds(20, 27, 958, 667);
		contentPanel.add(panel);
		
		JButton btnGenerarArbolMinimo = new JButton("Generar Arbol Minimo");
		btnGenerarArbolMinimo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(kruskal==true) {
					JOptionPane.showMessageDialog(null, "El algoritmo de Kruskal ya fue aplicado");
				}
				else {				
				panel.removeAll();
				panel.setVisible(false);
				datos.arbolMinimo();
			    panel1= new lienzo(contentPanel,datos);
				panel1.setBounds(20, 27, 958, 667);
				contentPanel.add(panel1);
				kruskal=true;
			}
		}});
		btnGenerarArbolMinimo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnGenerarArbolMinimo.setBounds(250, 697, 249, 30);
		contentPanel.add(btnGenerarArbolMinimo);
		
		JButton btnNewButton_1 = new JButton("Dividir");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (kruskal==false) {
					JOptionPane.showMessageDialog(null, "Debe aplicar primero el algoritmo de Kruskal");
				}
				if(dividido==datos.Personas.length-1) {
					JOptionPane.showMessageDialog(null, "El grafo no se puede dividir m�s");
				}
				else {
					panel1.removeAll();
					panel1.setVisible(false);
					datos.dividir();
				    panel1= new lienzo(contentPanel,datos);
					panel1.setBounds(20, 27, 958, 667);
					contentPanel.add(panel1);
					dividido++;	
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnNewButton_1.setBounds(583, 697, 133, 30);
		contentPanel.add(btnNewButton_1);
	}
}
