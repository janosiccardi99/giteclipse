package InterfazGrafica;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Negocio.GrafoPersonas;
import Negocio.Persona;

public class lienzo extends JPanel implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;
	int x = 0, y = 0;
	linea linea;
	Circulo circulo;
	Circulo desplazado = null;
	Random r1 = new Random();
	public Circulo[] ListCirculos;
	List<linea> ListArista = new ArrayList<linea>();
	List<Persona> Personas = new ArrayList<Persona>();
	JPanel panel;
	GrafoPersonas datos = new GrafoPersonas(x);

	public lienzo(JPanel contentPane, GrafoPersonas dato) {
		panel = contentPane;
		datos = dato;
		this.ListCirculos = new Circulo[datos.Personas.length];
		int[][] adyac = datos.grafo.getAdyacencia();
		for (int i = 0; i < adyac.length; i++) {
			for (int j = 0; j < adyac.length; j++) {
				if (ListCirculos[i] == null) {
					crearVertice(i);
				}
			}
		}
		crearArista(adyac);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setVisible(true);
		this.setDoubleBuffered(true);
	}

	public void anadirCirculo(int x, int y, String nombre) {
		for (int j = 0; j < ListCirculos.length; j++) {
			if (ListCirculos[j] == null) {
				int[] xy = verificarPosiciones(j, x, y);
				circulo = new Circulo(xy[0], xy[1], nombre);
				ListCirculos[j] = circulo;
				break;
			}
		}
		repaint();
		panel.repaint();
	}

	public int[] verificarPosiciones(int j, int x, int y) {	//verifica las posiciones con respecto al circulo anterior y genera nuevas en el caso de que esten cerca
			while (j != 0 && (Math.abs(ListCirculos[j-1].getX() - x) < 100 || Math.abs(ListCirculos[j-1].getY() - y) < 100)) {
				x = r1.nextInt(875) + 50;
				y = r1.nextInt(550) + 50;
			}		
		int[] salida = { x, y };
		return salida;
	}

	public void crearVertice(int i) {
		String nombre = datos.Personas[i].getNombre();
		anadirCirculo(r1.nextInt(875) + 50, r1.nextInt(550) + 50, nombre);
	}

	public void anadirLinea(int x, int y, int similar) {
		try {
			linea = new linea(ListCirculos[x], ListCirculos[y], similar);
			this.ListArista.add(linea);
			repaint();
			panel.repaint();
		} catch (IndexOutOfBoundsException e) {
			JOptionPane.showMessageDialog(null, "No se encontro circulo");
		}
	}

	public void crearArista(int[][] adyacencia) {
		for (int x = 0; x < adyacencia.length; x++) {
			for (int y = 0; y < adyacencia.length; y++) {
				if (adyacencia[x][y] != -1) {
					anadirLinea(x, y, datos.indiceDeSimilaridad(datos.Personas[x],datos.Personas[y]));
				}
			}
		}
	}

	public void mouseDragged(MouseEvent e) {
		if (desplazado == null) {
			for (Circulo f : ListCirculos) {
				if (f.desplazadoPor(e.getPoint())) {
					desplazado = f;
				}
				x = e.getPoint().x;
				y = e.getPoint().y;
				repaint();
				panel.repaint();
			}
		} else {
			desplazado.transladar(e.getPoint().x - x, e.getPoint().y - y);
			x = e.getPoint().x;
			y = e.getPoint().y;
			repaint();
			panel.repaint();
		}
	}

	public void mouseMoved(MouseEvent e) {
		desplazado = null;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		for (linea f : ListArista) {
			f.painter(g);
		}

		for (Circulo f : ListCirculos) {
			f.painter(g, this);
		}

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
}
