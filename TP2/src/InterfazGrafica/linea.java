package InterfazGrafica;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class linea{

    Circulo inicio;
    Circulo fin;
    Color color= Color.BLACK;
    int similaridad;

    public linea(Circulo inicio, Circulo fin,int similar) {
        this.inicio=inicio;
        this.fin=fin;
        similaridad=similar;
    }

	public void painter(Graphics g) {
    	int x1=inicio.getX();
    	int x2=fin.getX();
    	int y1=inicio.getY();
    	int y2=fin.getY();     
    	Graphics2D g2d = (Graphics2D)g;
        g2d.setStroke(new BasicStroke(5));  
        g2d.setColor(Color.RED);
        g2d.drawLine(inicio.getX(), inicio.getY(), fin.getX(), fin.getY());
        g.setColor(Color.BLACK);
        g.setFont(new Font("Comic Sans MS", Font.BOLD, 22));
        g.drawString(String.valueOf(similaridad),((x2+x1)/2)+10,((y1+y2)/2)+10);
        }

    public void setColor(Color color) {
        this.color = color;
    }

    public Circulo getFfinal() {
        return fin;
    }

    public Circulo getInicial() {
        return inicio;
    }
    
}