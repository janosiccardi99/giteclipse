package Negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class AGMTest {

	   @Test
	    public void arbolMinimoTest() {
	        GrafoPersonas g = new GrafoPersonas(3);
	        g.agregarPersona("a", 1, 2, 3, 4, 0);
	        g.agregarPersona("b", 2, 3, 5, 4, 1);
	        g.agregarPersona("c", 3, 2, 1, 4, 2);
	        g.setearGrafo();
	        g.arbolMinimo();
	        int[][] a = { { -1, 4, 4 }, { 4, -1, -1 }, { 4, -1, -1 } };
	        assertEquals(g.getGrafo().getAdyacencia(), a);
	    }

}
