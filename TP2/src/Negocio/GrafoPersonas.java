package Negocio;
import java.util.Arrays;

public class GrafoPersonas {
	public Grafo grafo;
	public Persona[] Personas;
	int cantidadPersonas;
	Estadisticas estadisticas;
	
	
	public GrafoPersonas(int cantidadPersonas) {		
		this.grafo = new Grafo(cantidadPersonas);
		this.Personas= new Persona[cantidadPersonas];
		this.cantidadPersonas= cantidadPersonas;	
		this.estadisticas=new Estadisticas();
	}
	
	public void agregarPersona(String nombre, int d, int m,int e,int c, int indice) {
		Persona persona=new Persona(nombre,d,m,e,c);
		estadisticas.sumaIntereses(d, m, e, c);				
		Personas[indice]= persona;	
	}
	
	public void setearGrafo() {
		for(int i=0; i<Personas.length; i++) {
			for(int j=0; j<Personas.length; j++) {
				if (i!=j && !grafo.existeArista(i, j)) {		
					int indice= indiceDeSimilaridad(Personas[i],Personas[j]);
					grafo.agregarArista(i, j,indice);					
					estadisticas.sumaIndice(indice);
					estadisticas.similaridades(indice,Personas[i].getNombre(),Personas[j].getNombre());				
				}
			}			
		}		
		
	}
	public int indiceDeSimilaridad(Persona i, Persona j) {
		int d=i.getDeporteI()- j.getDeporteI();
		int m=i.getMusicaI() - j.getMusicaI();
		int e=i.getEspectaculoI() - j.getEspectaculoI();
		int c=i.getCienciaI() - j.getCienciaI();
		
		return Math.abs(d)+Math.abs(m)+Math.abs(e)+Math.abs(c);		
	}
	
	public Estadisticas getEstadisticas() {
		return estadisticas;
	}

	public Persona[] getPersonas() {
		return Personas;
	}

	public void setPersonas(Persona[] personas) {
		Personas = personas;
	}

	@Override
	public String toString() {
		return "[" + Arrays.toString(Personas) + "]";
	}

	public void dividir() {
		grafo.dividir();				
		
	}
	public void arbolMinimo() {
		AGM agm= new AGM(grafo);		
		grafo.setAdyacencia(agm.arbolMinimo().getAdyacencia());
	}

	public Grafo getGrafo() {
		return grafo;
	}

	public void setGrafo(Grafo grafo) {
		this.grafo = grafo;
	}

	
}
