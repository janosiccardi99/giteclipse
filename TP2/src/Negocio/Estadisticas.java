package Negocio;

import java.util.HashMap;

public class Estadisticas {
	float suma;float sumaD;float sumaM;float sumaE;float sumaC;
	float cont;
	String iAlto;String jAlto;String iBajo;String jBajo;
	float masI=Integer.MIN_VALUE;float menosI=Integer.MAX_VALUE;
	String masInteresante;String menosInteresante;
	int masAlto=Integer.MIN_VALUE;int masBajo=Integer.MAX_VALUE;
	
	public Estadisticas() {		
	}
	
	void sumaIntereses(int d, int m, int e, int c) {
		sumaD+=d;
		sumaM+=m;
		sumaE+=e;
		sumaC+=c;
	}
	void similaridades(int indice,String i, String j) {		
		if(masAlto<indice) {
			masAlto=indice;	
			iAlto=i;
			jAlto=j;
			
		}
		if(masBajo>indice) {
			masBajo=indice;
			iBajo=i;
			jBajo=j;
		}
	}

	void sumaIndice(int indice) {
		suma+=indice;
		cont++;
	}
	public String dameEstadisticas(Persona[] Personas) {	
		return "Estadisticas: \n Promedio de similaridad: "+ (float)(suma/cont)+"\n "+ promedioDeInteres(Personas) +
				"\n Tema mas interesante: "+masInteresante+"\n Tema menos interesante: "+menosInteresante+"\n Indice de similaridad mas bajo: "+ masBajo+
				"("+iBajo+" y "+jBajo+")\n Indice de similaridad mas alto: "+masAlto+"("+iAlto+" y "+jAlto+")"; 
		
	}
	
	private String promedioDeInteres(Persona[] Personas) {		
		float n=Personas.length;		
		HashMap<String,Float> promedios= new HashMap<String,Float>();
		promedios.put("Deporte", sumaD/n);	promedios.put("Musica",sumaM/n);promedios.put("Espectaculo", sumaE/n);promedios.put("Ciencia", sumaC/n);	
		for(String promedio:promedios.keySet()) {
			if(masI<promedios.get(promedio)) {
				masI=promedios.get(promedio);			
				masInteresante=promedio;				
			}
			if(menosI>promedios.get(promedio)) {
				menosI=promedios.get(promedio);
				menosInteresante=promedio;
			}			
		}
		return "Interes promedio de Deporte: "+(float)sumaD/n+"\n Interes promedio de Musica: "+(float)sumaM/n+
				"\n Interes promedio de Espectaculo: "+(float)sumaE/n+"\n Interes promedio de Ciencia: "+(float)sumaC/n;		
	}

}
